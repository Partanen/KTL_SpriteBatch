KTL SpriteBatch

An OpenGL 3.3 sprite batching implementation in C++.

Notes on performance
    The lesser the amount of different textures called after one another, the
    better the performance.
    Sorting can be a drawback. By default, std::sort is used to sort
    sprites by the draw() method's layer parameter. By calling begin() with the
    appropriate parameters, sorting is disabled.

Use and compiling
    To run, an OpenGL context initialized for version 3.3 is required. GLEW
    should also be initialized with the experimental flag set to true.
    The following is a simplified example of use. The 'examples' directory
    includes a more thorough, compilable example with initialization.

    #########################################################

    // 1: Initialize OpenGL and GLEW

    // 2: Load an image from disc

    KTL::Texture texture;
    KTL::Clip clip = {0, 0, image_width, image_width};
    KTL::SpriteBatch batch;

    texture.load(&image_pixel_array, image_width, image_height, KTL::RGBA);

    batch.create();

    while(1)
    {
        batch.begin();
        batch.draw(texture, clip, 25, 25, 0);
        batch.draw(texture, clip, 40, 256, 0);
        batch.end();

        glClearColor(0, 0, 0, 0);
        glClear(GL_COLOR_BUFFER_BIT);
        // OpenGL buffer swap
    }

    texture.free()
    batch.dispose();

    // exit program

    #########################################################


