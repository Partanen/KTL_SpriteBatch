#include <cstdio>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <cmath>
#include "../../src/SpriteBatch.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

KTL::Texture loadTextureFromFileSDL(const char *path);

void init(SDL_Window **window,
          SDL_GLContext *context,
          KTL::SpriteBatch &batch,
          KTL::Texture &spritesheet);

void update(bool &running);

void render(SDL_Window *window,
            KTL::SpriteBatch &batch,
            KTL::Texture &spritesheet,
            KTL::Clip &tile_clip1,
            KTL::Clip &tile_clip2);

void clean(SDL_Window **window,
           SDL_GLContext *gl_context,
           KTL::SpriteBatch &batch,
           KTL::Texture &spritesheet);

KTL::Texture
loadTextureFromFileSDL(const char *path)
{
	SDL_Surface *surface = IMG_Load(path);
	KTL::Texture tex;

    if (!surface)
    {
        printf("Failed to create an SDL surface from path %s. SDL_Error: %s\n", path, SDL_GetError());
    }
    else
    {
        KTL::ColorFormat color_format = surface->format->BytesPerPixel == 4 ? KTL::RGBA : KTL::RGB;
        tex.load(surface->pixels, surface->w, surface->h, color_format);
        SDL_FreeSurface(surface);
    }

    return tex;
}

void
init(SDL_Window **window, SDL_GLContext *gl_context, KTL::SpriteBatch &batch, KTL::Texture &spritesheet)
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    *window = SDL_CreateWindow("SDL2 example",
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               SCREEN_WIDTH, SCREEN_HEIGHT,
                               SDL_WINDOW_OPENGL);

    *gl_context = SDL_GL_CreateContext(*window);

    SDL_GL_SetSwapInterval(0);
    glewExperimental = GL_TRUE;
    glewInit();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
        printf("Failed to initialize SDL_Image. SDL_Error: %s\n", SDL_GetError());

    spritesheet = loadTextureFromFileSDL("spritesheet.png");

    batch.create(); // initialize the sprite batch
}

void
render(SDL_Window *window,
       KTL::SpriteBatch& batch,
       KTL::Texture &spritesheet,
       KTL::Clip &tile_clip1,
       KTL::Clip &tile_clip2)
{
    glClearColor(0.129, 0.463, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    int mid_x = SCREEN_WIDTH/2  - 16;
    int mid_y = SCREEN_HEIGHT/2 - 16;

    uint32_t ticks  = SDL_GetTicks();
    float floating  = 2 * (float)std::sin(ticks * 1.75 * M_PI / 5);
    int y_add       = (int)floating;

    batch.begin(KTL_DEFAULT_SHADER_PROGRAM, 0.7f, {1.0f, 1.0f}, false, NULL);
        batch.draw(spritesheet, tile_clip1, mid_x, mid_y - 100 + y_add, 0);
        batch.draw(spritesheet, tile_clip2, mid_x-16, mid_y - 100 + 8 - y_add, 1);
        batch.draw(spritesheet, tile_clip2, mid_x+16, mid_y - 100 + 8 + y_add, 1);
        batch.draw(spritesheet, tile_clip1, mid_x-32, mid_y - 100 + 16 - y_add, 2);
        batch.draw(spritesheet, tile_clip1, mid_x, mid_y - 100 + 16 + y_add, 2);
        batch.draw(spritesheet, tile_clip1, mid_x+32, mid_y - 100 + 16 - y_add, 2);
        batch.draw(spritesheet, tile_clip2, mid_x-48, mid_y - 100 + 24 + y_add, 3);
        batch.draw(spritesheet, tile_clip2, mid_x-16, mid_y - 100 + 24 - y_add, 3);
        batch.draw(spritesheet, tile_clip2, mid_x+16, mid_y - 100 + 24 + y_add, 3);
        batch.draw(spritesheet, tile_clip2, mid_x+16, mid_y - 100 + 24 - y_add, 3);
        batch.draw(spritesheet, tile_clip2, mid_x+48, mid_y - 100 + 24 + y_add, 3);
        batch.draw(spritesheet, tile_clip1, mid_x-32, mid_y - 100 + 32 - y_add, 4);
        batch.draw(spritesheet, tile_clip1, mid_x, mid_y - 100 + 32 + y_add, 4);
        batch.draw(spritesheet, tile_clip1, mid_x+32, mid_y - 100 + 32 - y_add, 4);
        batch.draw(spritesheet, tile_clip2, mid_x-16, mid_y - 100 + 40 + y_add, 5);
        batch.draw(spritesheet, tile_clip2, mid_x+16, mid_y - 100 + 40 - y_add, 5);
        batch.draw(spritesheet, tile_clip1, mid_x, mid_y - 100 + 48 + y_add, 6);
    batch.end();

    SDL_GL_SwapWindow(window);
}


void
update(bool &running)
{
    SDL_Event event;

    while(SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE)
                    running=false;
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
                    glViewport(0, 0, event.window.data1, event.window.data2);
                break;
        }
    }
}


void clean(SDL_Window **window,
           SDL_GLContext *gl_context,
           KTL::SpriteBatch &batch,
           KTL::Texture &spritesheet)
{
    spritesheet.free();
    batch.dispose();
    SDL_GL_DeleteContext(*gl_context);
    SDL_DestroyWindow(*window);
}

SDL_Window          *WINDOW;
SDL_GLContext       GL_CONTEXT;
KTL::SpriteBatch    BATCH;

KTL::Texture        spritesheet;
KTL::Clip           tile_clip1 = { 0,  0, 32, 32};
KTL::Clip           tile_clip2 = { 0, 32, 32, 32};

int main(int argc, char *argv[])
{
    init(&WINDOW, &GL_CONTEXT, BATCH, spritesheet);

    bool running = true;

    while (running)
    {
        update(running);
        render(WINDOW, BATCH, spritesheet, tile_clip1, tile_clip2);
    }

    clean(&WINDOW, &GL_CONTEXT, BATCH, spritesheet);

    return 0;
}
