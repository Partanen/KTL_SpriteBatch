#pragma once
#include <vector>
#include <GL/glew.h>

namespace KTL
{

/*  The maximum amount of sprite data to be sent to
 *  the GPU at one time. */
#define KTL_BATCH_SIZE 2048
#define KTL_DEFAULT_SHADER_PROGRAM 0

struct Clip {int x, y, w, h;};
struct FPoint {float x, y;};
struct FColorRGBA {float r, g, b, a;};

/*  Pixels per byte */
enum ColorFormat
{
    RGB  = GL_RGB,
    RGBA = GL_RGBA
};

enum TextureWrapMode
{
    REPEAT          = GL_REPEAT,
    MIRRORED_REPEAT = GL_MIRRORED_REPEAT,
    CLAMP_TO_EDGE   = GL_CLAMP_TO_EDGE,
    CLAMP_TO_BORDER = GL_CLAMP_TO_BORDER
};

struct Texture
{
    Texture();

    /* A constructor that calls load() with the given arguments */
    Texture(void *pixels,
            int width,
            int height,
            ColorFormat color_format = RGBA,
            TextureWrapMode wrap_mode = REPEAT);

    /* Load or update an existing texture. If updating, the method
     * will automatically free the old texture */
    GLuint load(void *pixels,   // Pointer to an array of an image's pixel data.
                int width,      // Image original width
                int height,     // Image original height
                ColorFormat color_format = RGBA,    // Bytes per pixel in the given data
                TextureWrapMode wrap_mode = REPEAT);

    /* Free the texture from GPU memory. */
    void free();

    GLuint ID = 0;
    uint32_t width  = 0;
    uint32_t height = 0;
    ColorFormat color_format;
};

enum ShaderType
{
    VERTEX_SHADER   = GL_VERTEX_SHADER,
    FRAGMENT_SHADER = GL_FRAGMENT_SHADER
};

/*  Shorthand functions for loading new shaders.
 *  Simply pass in the GLSL code */
GLuint createShader(ShaderType shader_type,
                    const char * shader_code);
GLuint createShaderProgram(GLuint vertex_shader,
                           GLuint fragment_shader);
void deleteShader(GLuint shader);
void deleteShaderProgram(GLuint shader_program);


enum Flip
{
    FLIP_NONE,
    FLIP_HORIZONTAL,
    FLIP_VERTICAL,
    FLIP_BOTH
};


class SpriteBatch
{
    public:
        SpriteBatch();
        ~SpriteBatch();

        /* Initialize the batch for use. initial_size is the
         * starting batch size. If more sprites are queued,
         * the batch is automatically enlarged. */
        bool create(unsigned int initial_size = 256);

        void begin();

        void begin(GLuint shader_program, // Pass in 0 to use the default shader
                   float alpha,           // Multiply all queued sprites' alpha by this value
                   FPoint scale,
                   bool sort = true,      // If sorting is enabled, sprites with a lower layer are drawn first
                   const Clip *viewport = NULL); // Projection viewport - if NULL, will use OpenGL's own 

        void draw(Texture& texture,
                  Clip& clip,
                  int x, int y, int layer);

        void drawScaled(Texture& texture,
                        Clip& clip,
                        int x, int y, int layer,
                        FPoint scale);

        void drawFlipped(Texture& texture,
                         Clip& clip,
                         int x, int y, int layer,
                         Flip flip);

        /*  Colored drawing multiplies the texture's colors by the
         *  input color. This can be used to apply alpha alone,
         *  by passing in {1.0f, 1.0f, 1.0f, desired_alpha} */
        void drawTinted(Texture& texture,
                        Clip& clip,
                        int x, int y, int layer,
                        FColorRGBA& color);

        void drawRotated(Texture& texture,
                         Clip& clip,
                         int x, int y, int layer,
                         float rotation);

        void drawScaledFlipped(Texture& texture,
                               Clip& clip,
                               int x, int y, int layer,
                               FPoint scale,
                               Flip flip);

        void drawScaledRotated(Texture& texture,
                               Clip& clip,
                               int x, int y, int layer,
                               FPoint scale,
                               float rot);

        void drawScaledTinted(Texture& texture,
                              Clip& clip,
                              int x, int y, int layer,
                              FPoint scale,
                              FColorRGBA& color);

        void drawFlippedRotated(Texture& texture,
                                Clip& clip,
                                int x, int y, int layer,
                                Flip flip,
                                float rot);

        void drawFlippedTinted(Texture& texture,
                               Clip& clip,
                               int x, int y, int layer,
                               Flip flip,
                               FColorRGBA& color);

        void drawTintedRotated(Texture& texture,
                               Clip& clip,
                               int x, int y, int layer,
                               FColorRGBA& color,
                               float rot);

        void drawScaledTintedRotated(Texture& texture,
                                     Clip& clip,
                                     int x, int y, int layer,
                                     FPoint scale,
                                     FColorRGBA& color,
                                     float rot);

        void drawScaledFlippedTinted(Texture& texture,
                                     Clip& clip,
                                     int x, int y, int layer,
                                     FPoint scale,
                                     Flip flip,
                                     FColorRGBA& color);

        void drawFlippedTintedRotated(Texture& texture,
                                      Clip& clip,
                                      int x, int y, int layer,
                                      Flip flip,
                                      FColorRGBA& color,
                                      float rot);

        void drawScaledFlippedTintedRotated(Texture& texture,
                                            Clip& clip,
                                            int x, int y, int layer,
                                            FPoint scale,
                                            Flip flip,
                                            FColorRGBA& color,
                                            float rot);

        void end();

        /* Resize the sprite buffer, calling std::vector.clear(),
         * hence causing a reallocation. Fails if called between
         * begin() and end(). Returns the new size.*/
        size_t resizeBuffer(unsigned int new_size);
        size_t getBufferSize() const;

        /* Free any resources used by the batch from the GPU and memory. */
        void dispose();

        struct SpriteBatchItem
        {
            GLuint texture;
            GLfloat position[2];
            GLfloat dimensions[2];
            GLfloat clip[4];
            GLfloat color[4];
            GLfloat rotation;
            GLfloat rot_origin[2];
            int layer;
        };

    private:
        bool is_initialized;
        bool batching_started;

        unsigned int sprite_count;

        GLuint shader;
        GLuint override_shader;
        GLuint vao;
        GLuint vbo;
        GLuint ebo;

        GLfloat scale_x;
        GLfloat scale_y;
        GLfloat alpha;
        bool alpha_changed;
        bool sorting;
        bool override_viewport;
        GLint or_viewport[4];

        std::vector<SpriteBatchItem> sprite_buffer; //Starts out with 256 slots
        GLfloat vertex_buffer[KTL_BATCH_SIZE * 32];
};

} //End of namespace
