#include <cmath>
#include <algorithm>
#include "SpriteBatch.h"

GLuint
KTL::createShader(ShaderType shader_type,
                  const char *shader_code)
{
    GLuint shader_ID = glCreateShader(shader_type);

    glShaderSource(shader_ID, 1, &shader_code, NULL);
    glCompileShader(shader_ID);

    return shader_ID;
}

GLuint
KTL::createShaderProgram(GLuint vertex_shader,
                         GLuint fragment_shader)
{
    //Todo: error checking...
    GLuint shader = glCreateProgram();

    glAttachShader(shader, vertex_shader);
    glAttachShader(shader, fragment_shader);
    glLinkProgram(shader);
    glDetachShader(shader, vertex_shader);
    glDetachShader(shader, fragment_shader);

    return shader;
}

void
KTL::deleteShader(GLuint shader)
{
    glDeleteShader(shader);
}

void
KTL::deleteShaderProgram(GLuint shader_program)
{
    glDeleteProgram(shader_program);
}


namespace KTL
{
static inline void
inlRotatePixel(float *x, float *y,
               float& orig_x, float& orig_y,
               float& angle)
{
    float trans_x = *x - orig_x;
    float trans_y = *y - orig_y;

    float rot_x = trans_x *cos(angle) - trans_y *sin(angle);
    float rot_y = trans_x *sin(angle) + trans_y *cos(angle);

    *x = rot_x + orig_x;
    *y = rot_y + orig_y;
}


static inline bool
sortFunc(KTL::SpriteBatch::SpriteBatchItem& item1,
         KTL::SpriteBatch::SpriteBatchItem& item2)
{
    return item1.layer < item2.layer;
}

} // End of namespace

#define SET_ITEM_DEFAULT_DIMENSIONS(item, clip)\
    item.dimensions[0] = (GLfloat)clip.w;\
    item.dimensions[1] = (GLfloat)clip.h;

#define SET_ITEM_SCALED_DIMENSIONS(item, clip, scale)\
    item.dimensions[0] = (GLfloat)clip.w *scale.x;\
    item.dimensions[1] = (GLfloat)clip.h *scale.y;

#define SET_ITEM_DEFAULT_COLOR(item)\
    item.color[0] = 1.0f;\
    item.color[1] = 1.0f;\
    item.color[2] = 1.0f;\
    item.color[3] = 1.0f;

#define SET_ITEM_MOD_COLOR(item, color)\
    item.color[0] = (GLfloat)color.r;\
    item.color[1] = (GLfloat)color.g;\
    item.color[2] = (GLfloat)color.b;\
    item.color[3] = (GLfloat)color.a;

#define SET_ITEM_DEFAULT_CLIP(item, texture, clip)\
    item.clip[0] = (GLfloat)clip.x / (GLfloat)texture.width;\
    item.clip[1] = (GLfloat)clip.y / (GLfloat)texture.height;\
    item.clip[2] = ((GLfloat)clip.x + (GLfloat)clip.w) / (GLfloat)texture.width;\
    item.clip[3] = ((GLfloat)clip.y + (GLfloat)clip.h) / (GLfloat)texture.height;

#define SET_ITEM_FLIPPED_CLIP(item, flip, texture, clip)\
    switch (flip)\
    {\
    case FLIP_NONE:\
        item.clip[0] = (GLfloat)clip.x / (GLfloat)texture.width;\
        item.clip[1] = (GLfloat)clip.y / (GLfloat)texture.height;\
        item.clip[2] = (GLfloat)(clip.x + clip.w) / (GLfloat)texture.width;\
        item.clip[3] = (GLfloat)(clip.y + clip.h) / (GLfloat)texture.height;\
        break;\
    case FLIP_HORIZONTAL:\
        item.clip[0] = (GLfloat)(clip.x + clip.w) / (GLfloat)texture.width;\
        item.clip[1] = (GLfloat)clip.y / (GLfloat)texture.height;\
        item.clip[2] = (GLfloat)clip.x / (GLfloat)texture.width;\
        item.clip[3] = (GLfloat)(clip.y + clip.h) / (GLfloat)texture.height;\
        break;\
    case FLIP_VERTICAL:\
        item.clip[0] = (GLfloat)clip.x / (GLfloat)texture.width;\
        item.clip[1] = (GLfloat)(clip.y + clip.h) / (GLfloat)texture.height;\
        item.clip[2] = (GLfloat)(clip.x + clip.w) / (GLfloat)texture.width;\
        item.clip[3] = (GLfloat)clip.y / (GLfloat)texture.height;\
        break;\
    case FLIP_BOTH:\
        item.clip[0] = (GLfloat)clip.x / (GLfloat)texture.width;\
        item.clip[1] = (GLfloat)clip.y / (GLfloat)texture.height;\
        item.clip[2] = (GLfloat)(clip.x + clip.w) / (GLfloat)texture.width;\
        item.clip[3] = (GLfloat)(clip.y + clip.h) / (GLfloat)texture.height;\
        break;\
    }

KTL::SpriteBatch::SpriteBatch()
{
    is_initialized     = false;
    batching_started   = false;
    sprite_count = 0;
}

/* Initialize the batch */
bool
KTL::SpriteBatch::create(unsigned int initial_size)
{
    if (is_initialized) return is_initialized;

    alpha_changed = true;
    batching_started = false;

    sprite_buffer = std::vector<SpriteBatchItem>(initial_size);

    //Create the default shader program
    const char *vertex_code =
        "#version 330 core\n"

        "layout (location = 0) in vec4 position;\n"
        "layout (location = 1) in vec4 in_tint;\n"

        "out vec2 tex_pos;\n"
        "out vec4 tint;\n"

        "uniform mat4 projection;\n"

        "void main(void)\n"
        "{\n"
            "gl_Position = projection * vec4(position.x, position.y, 0.0f, 1.0f);\n"
            "tex_pos = vec2(position.z, position.w);\n"
            "tint = in_tint;\n"
        "}\n";

    const char *fragment_code =
        "#version 330 core\n"

        "in vec2 tex_pos;\n"
        "in vec4 tint;\n"

        "out vec4 color;\n"

        "uniform sampler2D tex;\n"
        "uniform float global_alpha;\n"

        "void main(void)\n"
        "{\n"
            "color = vec4(tint.x, tint.y, tint.z, global_alpha * tint.w) * texture(tex, tex_pos);\n"
        "}";

    GLuint vertex_shader    = createShader(VERTEX_SHADER, vertex_code);
    GLuint fragment_shader  = createShader(FRAGMENT_SHADER, fragment_code);

    shader = createShaderProgram(vertex_shader, fragment_shader);

    deleteShader(vertex_shader);
    deleteShader(fragment_shader);

    glUseProgram(shader);

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);

        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(4 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);

        glBufferData(GL_ARRAY_BUFFER, KTL_BATCH_SIZE * (32 * sizeof(GLfloat)), vertex_buffer, GL_DYNAMIC_DRAW);

        //Element array buffer
        GLint indices[6] = {0, 1, 2, 2, 3, 1};
        GLint *indice_array = new GLint[KTL_BATCH_SIZE * 6];

        for (int i = 0; i < KTL_BATCH_SIZE; ++i)
            for (int j = 0; j < 6; ++j)
                indice_array[i * 6 + j] = indices[j] + i * 4;

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, KTL_BATCH_SIZE * 6 * sizeof(GLint), indice_array, GL_DYNAMIC_DRAW);

        delete [] indice_array;

        glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    is_initialized = true;
    return is_initialized;
}

void
KTL::SpriteBatch::begin()
{
    if (!is_initialized || batching_started) return;

    sprite_count = 0;
    batching_started = true;
    override_shader = 0;
    override_viewport = false;
    sorting = true;

    scale_x = 1.0f;
    scale_y = 1.0f;

    if (alpha != 1.0f) alpha_changed = true;
    alpha = 1.0f;
}

/* Begin drawing a new batch with extended parameters */
void
KTL::SpriteBatch::begin(GLuint or_shader,
                        float global_alpha,
                        FPoint scale,
                        bool sort,
                        const Clip *viewp)
{
    if (!is_initialized || batching_started) return;

    sprite_count = 0;
    batching_started = true;
    override_shader = or_shader;
    sorting = sort;

    scale_x = (GLfloat)scale.x;
    scale_y = (GLfloat)scale.y;
    if (alpha != global_alpha) alpha_changed = true;
    alpha = global_alpha;

    if (viewp)
    {
        or_viewport[0] = viewp->x;
        or_viewport[1] = viewp->y;
        or_viewport[2] = viewp->w;
        or_viewport[3] = viewp->h;
        override_viewport = true;
    }
    else
        override_viewport = false;
}

#define DRAW_FUNC_UPPER_BODY\
    if (!batching_started) return;\
    if (sprite_count+1 > sprite_buffer.size())\
        sprite_buffer.push_back(SpriteBatchItem());\
    SpriteBatchItem& item = sprite_buffer[sprite_count];\
    item.texture = texture.ID;\
    item.position[0] = (GLfloat)x;\
    item.position[1] = (GLfloat)y;\
    item.layer = layer;

void
KTL::SpriteBatch::draw(Texture& texture,
                       Clip& clip,
                       int x, int y, int layer)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_DEFAULT_DIMENSIONS(item, clip);
    SET_ITEM_DEFAULT_CLIP(item, texture, clip);
    SET_ITEM_DEFAULT_COLOR(item);
    item.rotation = 0.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawScaled(KTL::Texture& texture,
                             Clip& clip,
                             int x, int y, int layer,
                             FPoint scale)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_SCALED_DIMENSIONS(item, clip, scale);
    SET_ITEM_DEFAULT_CLIP(item, texture, clip);
    SET_ITEM_DEFAULT_COLOR(item);
    item.rotation = 0.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawFlipped(Texture& texture,
                              Clip& clip,
                              int x, int y, int layer,
                              Flip flip)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_DEFAULT_DIMENSIONS(item, clip);
    SET_ITEM_FLIPPED_CLIP(item, flip, texture, clip);
    SET_ITEM_DEFAULT_COLOR(item);
    item.rotation = 0.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawTinted(Texture& texture,
                             Clip& clip,
                             int x, int y, int layer,
                             FColorRGBA& color)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_DEFAULT_DIMENSIONS(item, clip);
    SET_ITEM_DEFAULT_CLIP(item, texture, clip);
    SET_ITEM_MOD_COLOR(item, color);
    item.rotation = 0.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawRotated(Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       float rot)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_DEFAULT_DIMENSIONS(item, clip);
    SET_ITEM_DEFAULT_CLIP(item, texture, clip);
    SET_ITEM_DEFAULT_COLOR(item);
    item.rotation = rot;
    item.rot_origin[0] = (GLfloat)x + (GLfloat)clip.w/2.0f;
    item.rot_origin[1] = (GLfloat)y + (GLfloat)clip.h/2.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawScaledFlipped(KTL::Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       FPoint scale,
                       Flip flip)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_SCALED_DIMENSIONS(item, clip, scale);
    SET_ITEM_FLIPPED_CLIP(item, flip, texture, clip);
    SET_ITEM_DEFAULT_COLOR(item);
    item.rotation = 0.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawScaledRotated(KTL::Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       FPoint scale,
                       float rot)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_SCALED_DIMENSIONS(item, clip, scale);
    SET_ITEM_DEFAULT_CLIP(item, texture, clip);
    SET_ITEM_DEFAULT_COLOR(item);
    item.rotation = rot;
    item.rot_origin[0] = (GLfloat)x + (GLfloat)clip.w/2.0f;
    item.rot_origin[1] = (GLfloat)y + (GLfloat)clip.h/2.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawScaledTinted(KTL::Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       FPoint scale,
                       FColorRGBA& color)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_SCALED_DIMENSIONS(item, clip, scale);
    SET_ITEM_DEFAULT_CLIP(item, texture, clip);
    SET_ITEM_MOD_COLOR(item, color);
    item.rotation = 0.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawScaledTintedRotated(KTL::Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       FPoint scale,
                       FColorRGBA& color,
                       float rot)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_SCALED_DIMENSIONS(item, clip, scale);
    SET_ITEM_DEFAULT_CLIP(item, texture, clip);
    SET_ITEM_MOD_COLOR(item, color);
    item.rotation = rot;
    item.rot_origin[0] = (GLfloat)x + (GLfloat)clip.w/2.0f;
    item.rot_origin[1] = (GLfloat)y + (GLfloat)clip.h/2.0f;
    ++sprite_count;
}


void
KTL::SpriteBatch::drawScaledFlippedTinted(Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       FPoint scale,
                       Flip flip,
                       FColorRGBA& color)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_SCALED_DIMENSIONS(item, clip, scale);
    SET_ITEM_FLIPPED_CLIP(item, flip, texture, clip);
    SET_ITEM_MOD_COLOR(item, color);
    item.rotation = 0.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawFlippedRotated(KTL::Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       Flip flip,
                       float rot)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_DEFAULT_DIMENSIONS(item, clip);
    SET_ITEM_FLIPPED_CLIP(item, flip, texture, clip);
    SET_ITEM_DEFAULT_COLOR(item);
    item.rotation = rot;
    item.rot_origin[0] = (GLfloat)x + (GLfloat)clip.w/2.0f;
    item.rot_origin[1] = (GLfloat)y + (GLfloat)clip.h/2.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawFlippedTinted(Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       Flip flip,
                       FColorRGBA& color)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_DEFAULT_DIMENSIONS(item, clip);
    SET_ITEM_FLIPPED_CLIP(item, flip, texture, clip);
    SET_ITEM_MOD_COLOR(item, color);
    item.rotation = 0.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawTintedRotated(Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       FColorRGBA& color,
                       float rot)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_DEFAULT_DIMENSIONS(item, clip);
    SET_ITEM_DEFAULT_CLIP(item, texture, clip);
    SET_ITEM_MOD_COLOR(item, color);
    item.rotation = rot;
    item.rot_origin[0] = (GLfloat)x + (GLfloat)clip.w/2.0f;
    item.rot_origin[1] = (GLfloat)y + (GLfloat)clip.h/2.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawFlippedTintedRotated(Texture& texture,
                       Clip& clip,
                       int x, int y, int layer,
                       Flip flip,
                       FColorRGBA& color,
                       float rot)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_DEFAULT_DIMENSIONS(item, clip);
    SET_ITEM_FLIPPED_CLIP(item, flip, texture, clip);
    SET_ITEM_MOD_COLOR(item, color);
    item.rotation = rot;
    item.rot_origin[0] = (GLfloat)x + (GLfloat)clip.w/2.0f;
    item.rot_origin[1] = (GLfloat)y + (GLfloat)clip.h/2.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::drawScaledFlippedTintedRotated(Texture& texture,
                                                 Clip& clip,
                                                 int x, int y, int layer,
                                                 FPoint scale,
                                                 Flip flip,
                                                 FColorRGBA& color,
                                                 float rot)
{
    DRAW_FUNC_UPPER_BODY
    SET_ITEM_SCALED_DIMENSIONS(item, clip, scale);
    SET_ITEM_FLIPPED_CLIP(item, flip, texture, clip);
    SET_ITEM_MOD_COLOR(item, color);
    item.rotation = rot;
    item.rot_origin[0] = (GLfloat)x + (GLfloat)clip.w/2.0f;
    item.rot_origin[1] = (GLfloat)y + (GLfloat)clip.h/2.0f;
    ++sprite_count;
}

void
KTL::SpriteBatch::end()
{
    if (!is_initialized
    || !batching_started
    || sprite_count < 1)
        return;

    if (sorting)
        std::sort(sprite_buffer.begin(), sprite_buffer.begin()+sprite_count, sortFunc);

	//Calculate the projection matrix by viewport dimensions
    GLint viewport[4];
    if (!override_viewport)
        glGetIntegerv(GL_VIEWPORT, viewport);
    else
    {
        viewport[0] = or_viewport[0];
        viewport[1] = or_viewport[1];
        viewport[2] = or_viewport[2];
        viewport[3] = or_viewport[3];
    }

    GLfloat projection[4][4] =
    {
        {
            2.0f / (GLfloat)(viewport[2] - viewport[0]),
            0.0f,
            0.0f,
            0.0f
        },

        {
            0.0f,
            2.0f / (GLfloat)(viewport[1] - viewport[3]),
            0.0f,
            0.0f
        },

        {
            0.0f,
            0.0f,
            -1.0f,
            0.0f
        },

        {
            - (GLfloat)(viewport[2] + viewport[0]) / (GLfloat)(viewport[2] - viewport[0]),
            - (GLfloat)(viewport[1] + viewport[3]) / (GLfloat)(viewport[1] - viewport[3]),
            0.0f,
            1.0f
        }
    };

    GLuint program = (override_shader == 0) ? shader : override_shader;
    glUseProgram(program);

    glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, &projection[0][0]);

    if (alpha_changed)
    {
        glUniform1f(glGetUniformLocation(program, "global_alpha"), alpha);
        alpha_changed = false;
    }

    glBindVertexArray(vao);

        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

        // How many batches to process
        unsigned int num_batches = sprite_count / KTL_BATCH_SIZE;

        for (unsigned int batch_num = 0; batch_num < num_batches + 1; ++batch_num)
        {
            unsigned int num_sprites = sprite_count - batch_num * KTL_BATCH_SIZE;
            if (num_sprites > KTL_BATCH_SIZE) num_sprites = KTL_BATCH_SIZE;

            for (unsigned int i = batch_num * KTL_BATCH_SIZE; i < batch_num * KTL_BATCH_SIZE + num_sprites; ++i)
            {
                SpriteBatchItem& item = sprite_buffer[i];

                unsigned int fixed_i = (i - (i / KTL_BATCH_SIZE * KTL_BATCH_SIZE)) * 32;

                /* Top left */
                vertex_buffer[fixed_i     ] = scale_x * item.position[0];
                vertex_buffer[fixed_i +  1] = scale_y * item.position[1];
                vertex_buffer[fixed_i +  2] = item.clip[0];
                vertex_buffer[fixed_i +  3] = item.clip[1];
                vertex_buffer[fixed_i +  4] = item.color[0];
                vertex_buffer[fixed_i +  5] = item.color[1];
                vertex_buffer[fixed_i +  6] = item.color[2];
                vertex_buffer[fixed_i +  7] = item.color[3];
                /* Top right */
                vertex_buffer[fixed_i +  8] = scale_x * (item.position[0] + item.dimensions[0]);
                vertex_buffer[fixed_i +  9] = scale_y * item.position[1];
                vertex_buffer[fixed_i + 10] = item.clip[2];
                vertex_buffer[fixed_i + 11] = item.clip[1];
                vertex_buffer[fixed_i + 12] = item.color[0];
                vertex_buffer[fixed_i + 13] = item.color[1];
                vertex_buffer[fixed_i + 14] = item.color[2];
                vertex_buffer[fixed_i + 15] = item.color[3];
                /* Bottom left */
                vertex_buffer[fixed_i + 16] = scale_x * item.position[0];
                vertex_buffer[fixed_i + 17] = scale_y * (item.position[1]  + item.dimensions[1]);
                vertex_buffer[fixed_i + 18] = item.clip[0];
                vertex_buffer[fixed_i + 19] = item.clip[3];
                vertex_buffer[fixed_i + 20] = item.color[0];
                vertex_buffer[fixed_i + 21] = item.color[1];
                vertex_buffer[fixed_i + 22] = item.color[2];
                vertex_buffer[fixed_i + 23] = item.color[3];
                /* Bottom right */
                vertex_buffer[fixed_i + 24] = scale_x * (item.position[0] + item.dimensions[0]);
                vertex_buffer[fixed_i + 25] = scale_y * (item.position[1] + item.dimensions[1]);
                vertex_buffer[fixed_i + 26] = item.clip[2];
                vertex_buffer[fixed_i + 27] = item.clip[3];
                vertex_buffer[fixed_i + 28] = item.color[0];
                vertex_buffer[fixed_i + 29] = item.color[1];
                vertex_buffer[fixed_i + 30] = item.color[2];
                vertex_buffer[fixed_i + 31] = item.color[3];

                //Rotate if necessary
                if (item.rotation != 0.0f)
                {
                    inlRotatePixel(&vertex_buffer[fixed_i     ], &vertex_buffer[fixed_i +  1],
                                     item.rot_origin[0], item.rot_origin[1], item.rotation);
                    inlRotatePixel(&vertex_buffer[fixed_i +  8], &vertex_buffer[fixed_i +  9],
                                     item.rot_origin[0], item.rot_origin[1], item.rotation);
                    inlRotatePixel(&vertex_buffer[fixed_i + 16], &vertex_buffer[fixed_i + 17],
                                     item.rot_origin[0], item.rot_origin[1], item.rotation);
                    inlRotatePixel(&vertex_buffer[fixed_i + 24], &vertex_buffer[fixed_i + 25],
                                     item.rot_origin[0], item.rot_origin[1], item.rotation);
                }
            }

            unsigned int size_multip = sprite_count - batch_num * KTL_BATCH_SIZE;
            if (size_multip > KTL_BATCH_SIZE) size_multip = KTL_BATCH_SIZE;

            glBufferSubData(GL_ARRAY_BUFFER,                        // Target
                            0,                                      // Offset
                            size_multip * (32 * sizeof(GLfloat)),   // Size
                            &vertex_buffer[0]);                     // Data

            GLuint last_texture = sprite_buffer[batch_num * KTL_BATCH_SIZE].texture;
            int offset = 0;

            SpriteBatchItem *sprite;
            for (unsigned int i = 0; i < num_sprites; ++i)
            {
                sprite = &sprite_buffer[batch_num * KTL_BATCH_SIZE + i];

                if (sprite->texture != last_texture)
                {
                    glBindTexture(GL_TEXTURE_2D, last_texture);
                    glDrawElements(GL_TRIANGLES,
                                   (i - offset) * 6,
                                   GL_UNSIGNED_INT,
                                   (GLvoid *)(offset * 6 * sizeof(GLuint)));
                    offset = i;
                    last_texture = sprite->texture;
                }
            }

            glBindTexture(GL_TEXTURE_2D, last_texture);
            glDrawElements(GL_TRIANGLES,
                           (num_sprites - offset) * 6,
                           GL_UNSIGNED_INT,
                           (GLvoid*)(offset * 6 * sizeof(GLuint)));
        }
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    glUseProgram(0);
    batching_started = false;
}

size_t
KTL::SpriteBatch::resizeBuffer(unsigned int new_size)
{
    if (is_initialized && !batching_started)
    {
        sprite_buffer.clear();
        sprite_buffer.resize(new_size);
    }

    return sprite_buffer.size();
}

size_t
KTL::SpriteBatch::getBufferSize() const {return sprite_buffer.size();}

void
KTL::SpriteBatch::dispose()
{
    if (!is_initialized) return;

    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ebo);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(shader);

    sprite_buffer.clear();
    is_initialized = false;
    batching_started = false;
}


KTL::SpriteBatch::~SpriteBatch()
{
}

KTL::Texture::Texture()
{
    ID = 0;
    width  = 0;
    height = 0;
}

KTL::Texture::Texture(void *pixels,
                      int width,
                      int height,
                      ColorFormat color_format,
                      TextureWrapMode wrap_mode)
{
    load(pixels, width, height, color_format, wrap_mode);
}

GLuint
KTL::Texture::load(void *pixels,
                   int w,
                   int h,
                   ColorFormat color_format,
                   TextureWrapMode wrap_mode)
{
    if (ID != 0) free();

    width   = w;
    height  = h;
    this->color_format = color_format;

    glGenTextures(1, &ID);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ID);

    // Set magnifying filtering to nearest so the textures do not become blurred when scaled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode);

    glTexImage2D(GL_TEXTURE_2D, 0, color_format,
                 width, height,
                 0, color_format, GL_UNSIGNED_BYTE,
                 pixels);

    glBindTexture(GL_TEXTURE_2D, 0);

    return ID;
}

void
KTL::Texture::free()
{
    if (ID == 0) return;

    glDeleteTextures(1, &ID);

    ID = 0;
    width	= 0;
    height	= 0;
}
